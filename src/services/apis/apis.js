import { Observable, forkJoin } from 'rxjs';
import { getJson } from '../../helpers/reactive-api-client/reactive-api-client';
import { tap, map, switchMap, catchError } from 'rxjs/operators';

class ApiService {

    /**
     * @function
     * @description This call returns all payment accounts that are relevant the PSU on behalf of whom the AISP is connected.
     * @return {Observable} accounts
     */
    getAccounts() {
        return getJson(`/api/accounts`)
    }

    /**
     * @function
     * @description This call returns the balances and the transaction of a given account
     * @return {Observable} account
     */
    getAccountDetails(account) {
        return this.concatRequests(
            this.getBalances(account.resourceId),
            this.getTransactions(account.resourceId))
            .pipe(
            map(data => {
                account.balances = data[0];
                account.transactions = data[1];
                return account
            })
        )
    }

    getBalances(accountId){
        return getJson(`/api/accounts/balances?accountResourceId=${accountId}`);
    }

    getTransactions(accountId){
        return getJson(`/api/accounts/transactions?accountResourceId=${accountId}`);
    }

    concatRequests(...requests) {
        return forkJoin(requests)
    }
}

export const apiService = new ApiService();