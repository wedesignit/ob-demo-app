import * as React from 'react';
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import PrivateRoute from "../components/private-route/private-route";
import LoginView from "./login-view/login-view";
import LandingView from "./landing/landing";
import AccountsView from "./accounts-view/accounts-view";
import '../static/cool-background.png';
import TransactionsView from "./transactions-view/transactions-view";

class App extends React.Component {
    constructor() {
        super();
    }

    initRoutes() {
        return (
            <Switch>
                <Route path="/auth" component={LandingView}/>
                <Route path="/login" component={LoginView}/>
                <PrivateRoute exact path="/accounts" component={AccountsView}/>
                <PrivateRoute exact path="/transactions/:accountId" component={TransactionsView}/>
                <Redirect from="*" to="/login"/>
            </Switch>
        )
    }

    render() {
        return (
            <div id="main-container" style={{backgroundImage: "url('./assets/cool-background.png')"}}>
                <div className="ob-title">OPEN<span className="green">BANK</span></div>
                {this.initRoutes()}
            </div>
        )
    }
}

export default withRouter(App)