import * as React from 'react';
import * as PropTypes from 'prop-types';

import { apiService } from "../../services/apis/apis";
import { formatter } from "../../helpers/formatter/formatter";
import Spinner from "../../components/spinner/spinner";
import Menu from "../../components/menu/menu";
import '../../static/jobs.jpg';

export default class AccountsView extends React.Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            loadingMessage: '',
            loadingError: false
        }
    }

    componentWillMount() {
        this.init()
    }

    componentWillUnmount() {
        if (this.getAccountsSubscription) this.getAccountsSubscription.unsubscribe()
        if (this.getAccountsDetailsSub) this.getAccountsDetailsSub.unsubscribe()
    }

    init() {
        this.loadAccountsInfo()
    }

    loadAccountsInfo() {
        this.setState({
            isLoading: true,
            loadingMessage: 'loading accounts information',
            loadingError: false
        });
        this.getAccountsSubscription = apiService.getAccounts().subscribe(
            accounts => {
                const requests = [];
                if (accounts && accounts.length > 0) {
                    accounts.forEach(account => {
                        requests.push(
                            apiService.getAccountDetails(account)
                        )
                    });
                    this.getAccountsDetailsSub = apiService.concatRequests(...requests).subscribe(
                        accountsDetails => {
                            this.accounts = accountsDetails;
                            this.setState({
                                isLoading: false,
                                loadingMessage: '',
                                loadingError: false
                            })
                        },
                        err => {
                            if (err.response && err.response.status === 401) {
                                this.redirect('/login')
                            } else {
                                console.error(err);
                                this.setState({
                                    isLoading: false,
                                    loadingMessage: '',
                                    loadingError: true
                                })
                            }
                        }
                    )
                }
            },
            err => {
                if (err.response && err.response.status === 401) {
                    this.redirect('/login')
                } else {
                    console.error(err);
                    this.setState({
                        isLoading: false,
                        loadingMessage: '',
                        loadingError: true
                    })
                }
            }
        )
    }

    renderAccountItem(account) {
        return (
            <div className="account-box">
                <span className="account-type">
                    {account.cashAccountType === 'CACC' ? 'CURRENT ACCOUNT' : account.cashAccountTypes}
                </span>
                <span className="account-id">{formatter.formatIBAN(account.accountId.iban)}</span>
                <span className="account-balance">BALANCE</span>
                <span className="account-balance-amount">{account.balances[0].balanceAmount.amount}</span>
                <span className="account-balance-currency">{account.balances[0].balanceAmount.currency}</span>
                <button className="transactions-btn"
                        onClick={this.redirect.bind(this, `/transactions/${account.resourceId}`)}>
                    <i className="icofont icofont-exchange"/>TRANSACTIONS
                </button>
            </div>
        )
    }

    renderView() {
        const {name} = this.accounts[0];
        return (
            <div id="account-info-container">
                <Menu items={[{label: 'Sign out'}]}/>
                <div className="customer-info">
                    <div className="avatar">
                        <img alt={name} src="./assets/jobs.jpg"/>
                    </div>
                    <div className="customer-name">{name}</div>
                </div>
                <div className="wallet">
                    <h3>Wallet</h3>
                    <ul className="account-list">
                        {
                            this.accounts.map((item, index) => {
                                return <li key={index} className="account-list-item">{this.renderAccountItem(item)}</li>
                            })
                        }
                    </ul>
                    <i className="icofont icofont-circle"/>
                </div>
            </div>
        )
    }

    redirect(path) {
        this.props.history.push(path);
    }

    render() {
        const {isLoading, loadingMessage, loadingError} = this.state;
        if (isLoading) {
            return <Spinner text={loadingMessage}/>
        } else if (loadingError) {
            return <div>error</div>
        } else {
            return this.renderView()
        }
    }
}

AccountsView.propTypes = {
    match: PropTypes.object.isRequired,
    history: PropTypes.object
};